import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import KFold
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from scipy.stats.stats import pearsonr

def main():

	df_AQM = pd.read_csv('master_24OneHot.csv')	

	list_model_features = ['temp', 'rh', 'wd.sigma', 'wd', 'ws', 'VPH', 'cef', 'co_lagged', 'co_lagged_2']
	list_features = list_model_features + ['hod', 'dow', 'vphSource']
	
	#list_features = ['temp', 'rh', 'co', 'wd.sigma', 'wd', 'ws', 'hod', 'dow', 'vphSource', 'VPH', 'cef', 'co_lagged', 'co_lagged_2']

	df_AQM = pd.read_csv('master_24OneHot.csv', usecols = list_features)
		
	num_data_pts = df_AQM.shape[0]
	num_hours = 24
	num_weekdays = 7
	for i in range(num_hours):
		list_features.append('hod' + str(i+1))
	list_features.extend(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'])
	
	data_array = df_AQM.values

	dict_weekday = {'Monday': 0, 'Tuesday' : 1, 'Wednesday' : 2, 'Thursday' : 3, 'Friday' : 4, 'Saturday' : 5, 'Sunday' : 6}

	ohe_hod = [[0 for x in range(num_hours)] for y in range(num_data_pts)]
	ohe_dow = [[0 for x in range(num_weekdays)] for y in range(num_data_pts)]
	for index in range(num_data_pts):
		curr_hour = df_AQM['hod'][index]; curr_day = dict_weekday[df_AQM['dow'][index]]
		ohe_hod[index][curr_hour - 1] = 1	
		ohe_dow[index][curr_day] = 1
		
	data_array = np.delete(data_array, [6,7,8], axis = 1)
	data_array = preprocessing.scale(data_array, axis = 0)

	data_array = np.append(data_array, ohe_hod, axis = 1); data_array = np.append(data_array, ohe_dow, axis = 1)
	
	#rho_A = pearsonr(data_array[:,2], data_array[:,9])
	#print(rho_A)		

	Y_data = np.copy(data_array[:,2])
	X_data = np.delete(data_array, [2], axis = 1)

	print(X_data.shape, Y_data.shape)	
	reg_constant = np.linspace(0.001, 10, 50) 		# Set of allowable regularization constants  
	num_folds = 10
	kf = KFold(n_splits = num_folds, shuffle = False)
	for reg_lambda in reg_constant:
		avg_r2_lasso = 0; avg_r2_ridge = 0
		for train_index, test_index in kf.split(X_data, Y_data):
			X_train, X_test = X_data[train_index], X_data[test_index]
			Y_train, Y_test = Y_data[train_index], Y_data[test_index]
				
			clf_lasso = Lasso(reg_lambda); clf_ridge = Ridge(reg_lambda)
			clf_lasso.fit(X_train, Y_train); clf_ridge.fit(X_train, Y_train)
			r2_lasso = clf_lasso.score(X_test, Y_test); r2_ridge = clf_ridge.score(X_test, Y_test)
			avg_r2_lasso += r2_lasso; avg_r2_ridge += r2_ridge
		avg_r2_lasso = avg_r2_lasso / num_folds; avg_r2_ridge = avg_r2_ridge / num_folds		
		print('Ridge: ' + str(reg_lambda) + ' : ' + str(avg_r2_ridge))
		print('Lasso: ' + str(reg_lambda) + ' : ' + str(avg_r2_lasso)) 

	

	return



if __name__ == '__main__':
	main()
