import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import KFold
from sklearn.linear_model import Ridge
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt

def main():

	df_AQM = pd.read_csv('master_24OneHot.csv')	

	#list_model_features = ['temp', 'rh', 'wd.sigma', 'wd', 'ws', 'VPH', 'cef', 'co_lagged', 'co_lagged_2']
	list_model_features = ['temp', 'rh', 'ws', 'wd', 'VPH']
	list_features = list_model_features + ['hod', 'dow', 'vphSource', 'co']
	
	#df_AQM['hod'] = df_AQM['hod'].astype(int)
	df_AQM = pd.read_csv('master_24OneHot.csv', usecols = list_features)
	#df_AQM = df_AQM[(df_AQM['hod'] <= 21) & (df_AQM['hod'] >= 7)]
	df_AQM = df_AQM[(df_AQM['vphSource'] == 'True') & ((df_AQM['hod'] <= 23) & (df_AQM['hod'] >= 0))]
	df_AQM = df_AQM.reset_index(drop = True)
	print(df_AQM)	
	
	num_data_pts = df_AQM.shape[0]
	print(num_data_pts)
	num_hours = 24
	num_weekdays = 7

	Y_data = df_AQM[['co']].values
	X_data = df_AQM[list_model_features].values

	dict_weekday = {'Monday': 0, 'Tuesday' : 1, 'Wednesday' : 2, 'Thursday' : 3, 'Friday' : 4, 'Saturday' : 5, 'Sunday' : 6}

	ohe_hod = [[0 for x in range(num_hours)] for y in range(num_data_pts)]
	ohe_dow = [[0 for x in range(num_weekdays)] for y in range(num_data_pts)]
	for index in range(num_data_pts):
		curr_hour = df_AQM['hod'][index]; curr_day = dict_weekday[df_AQM['dow'][index]]
		ohe_hod[index][curr_hour - 1] = 1	
		ohe_dow[index][curr_day] = 1
		
	X_data = preprocessing.scale(X_data, axis = 0)
	Y_data = preprocessing.scale(Y_data, axis = 0)

	X_data = np.append(X_data, ohe_hod, axis = 1); X_data = np.append(X_data, ohe_dow, axis = 1)

	lambda_vec = []; test_vec = []	 
	reg_constant = np.linspace(0.001, 20, 100) 		# Set of allowable regularization constants  
	num_folds = 10
	kf = KFold(n_splits = num_folds, shuffle = False)
	for reg_lambda in reg_constant:
		avg_r2_ridge = 0
		for train_index, test_index in kf.split(X_data, Y_data):
			X_train, X_test = X_data[train_index], X_data[test_index]
			Y_train, Y_test = Y_data[train_index], Y_data[test_index]
				
			clf_ridge = Ridge(reg_lambda)
			clf_ridge.fit(X_train, Y_train)
			r2_ridge = clf_ridge.score(X_test, Y_test)
			avg_r2_ridge += r2_ridge
		avg_r2_ridge = avg_r2_ridge / num_folds		
		print('Ridge: ' + str(reg_lambda) + ' : ' + str(avg_r2_ridge))
		lambda_vec.append(reg_lambda); test_vec.append(avg_r2_ridge)

	fig, ax = plt.subplots()
	ax.scatter(lambda_vec, test_vec)
	ax.plot(lambda_vec, test_vec)
	ax.set_xlabel('Regularizer coefficient')
	ax.set_ylabel('R_square (test)')
	ax.set_title('Aggregate K-Fold prediction error with varying regularizer coefficient')
	fig.savefig('scatter.png')
	plt.close(fig)

	
	return



if __name__ == '__main__':
	main()
