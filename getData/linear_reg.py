import statsmodels.api as sm
import csv
from scipy.stats.stats import pearsonr   

def main():

	list_index = []
	filenames = ['SENS_TEMPERATURE.csv', 'SENS_RELATIVE_HUMIDITY.csv','SENS_CARBON_DIOXIDE.csv']
	
	for filename in filenames:
		curr_file = 'write' + filename
		with open(curr_file, 'r') as fp:
			reader = csv.reader(fp)
			list_index.append(list(reader))
	
	print(len(list_index[0]), len(list_index[1]), len(list_index[2]))
	num_ele = len(list_index[0])

	list_pollutant = []
	list_features = [[] for x in range(num_ele)]
	for ele in range(num_ele):
		list_features[ele].extend([float(list_index[0][ele][2]), float(list_index[1][ele][2])])
		list_pollutant.append(float(list_index[2][ele][2]))

	#print(list_features, list_pollutant)

	list_features = sm.add_constant(list_features)

	model = sm.OLS(list_pollutant, list_features)
	results = model.fit()
	print(results.summary())
	
	# Variance Inflation Factors due to covariates 
	list_temp = [list_features[i][1] for i in range(num_ele)]
	list_humid = [list_features[i][2] for i in range(num_ele)]
	list_temp_c = sm.add_constant(list_temp); list_humid_c = sm.add_constant(list_humid)

	print("\n\n")
	print(pearsonr(list_temp, list_humid))
	print("\n\n")

	model_temp = sm.OLS(list_temp, list_humid_c)
	results_temp = model_temp.fit()
	print(results_temp.summary())

	model_humid = sm.OLS(list_humid, list_temp_c)
	results_humid = model_humid.fit()
	print(results_humid.summary()) 
	
	



	return
		

if __name__ == '__main__':
	main()
