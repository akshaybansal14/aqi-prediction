import requests
import json
import time
import os
import sys
import base64
import requests
from datetime import datetime
from dateutil import tz


"""
    Enter time in GMT format
"""


startDate = "2018-10-20T18:30:00Z"
endDate = "2018-12-31T18:30:00Z"


"""
    Start of get data script
"""

#properties = ["SENS_LIGHT", "SENS_AIR_PRESSURE", "SENS_TEMPERATURE", "SENS_CARBON_DIOXIDE",
#"SENS_RELATIVE_HUMIDITY", "SENS_SOUND", "SENS_NITRIC_OXIDE", "SENS_ULTRA_VIOLET", "SENS_PM2P5",
#"SENS_PM10", "SENS_NITROGEN_DIOXIDE", "SENS_CARBON_MONOXIDE", "SENS_SULPHUR_DIOXIDE", "SENS_OZONE"]
properties = ["SENS_TEMPERATURE", "SENS_RELATIVE_HUMIDITY", "SENS_CARBON_DIOXIDE", "SENS_NITRIC_OXIDE", "SENS_PM2P5", 
"SENS_PM10", "SENS_NITROGEN_DIOXIDE", "SENS_CARBON_MONOXIDE", "SENS_SULPHUR_DIOXIDE", "SENS_OZONE"]


aqi_sub = ["SENS_PM2P5", "SENS_PM10", "SENS_NITROGEN_DIOXIDE", "SENS_CARBON_MONOXIDE", "SENS_SULPHUR_DIOXIDE", "SENS_OZONE"]


# Elcita Climo Credentials
uname = base64.b64encode(b"ELCITA").decode("UTF-8")
pwd = base64.b64encode(b"Climo@903").decode("UTF-8")

# IISC Bangalore Credentials
#uname = "SUlTQ19CQU5HQUxPUkU=" 
#pwd = "Q2xpbW9AOTAz"

from_zone = tz.tzutc()
to_zone = tz.tzlocal()

bosch_thing_headers = ""
aqi_url = ""
poll_url = ""
thingKey = ""
def authorize():
    global bosch_thing_headers, aqi_url, poll_url, thingKey
    bosch_auth_url = "http://52.28.187.167/services/api/v1/users/login"
    bosch_auth_headers = {"Content-Type":"application/json", "Accept":"application/json", "api_key":"apiKey"}
    bosch_auth_payload = {"password":pwd, "username":uname}
    auth_res = requests.post(url = bosch_auth_url, headers = bosch_auth_headers, data = json.dumps(bosch_auth_payload))
    print("Authorizing Data")
    print(auth_res)
    authToken = auth_res.json()["authToken"]
    OrgKey = auth_res.json()["OrgKey"]
    bosch_thing_url = "http://52.28.187.167/services/api/v1/getAllthings"
    bosch_thing_headers = {"Accept":"application/json", "api_key":"apiKey", "Authorization":authToken, "X-OrganizationKey":OrgKey}
    thing_res = requests.get(url = bosch_thing_url, headers = bosch_thing_headers)
    thingKey = thing_res.json()["result"][0]["thingKey"]
    print(thingKey)
    aqi_url = "http://52.28.187.167/services/api/v1/thing/" + thingKey + "/aqi/{propertyKey}/1/INDIA"
    poll_url = "http://52.28.187.167/services/api/v1/things/" + thingKey + "/{propertyKey}/" + startDate + "/" + endDate 

sens_data = {}
for key in properties:
    sens_data[key] = []
aqi = {}
for key in aqi_sub:
    aqi["AQI_" + key] = 0.0

authorize()
series = []
payload = {}

for p in properties:
    f = open(p+".csv",'a')
    try:
        r = requests.get(url=poll_url.replace("{propertyKey}", p), headers = bosch_thing_headers)
        if(r.json()["result"]["values"] is not None):
            sens_data[p] = r.json()["result"]["values"]
            for row in sens_data[p]:
                utc = datetime.strptime(row["ts"], "%Y-%m-%dT%H:%M:%SZ")
                utc = utc.replace(tzinfo=from_zone)
                ist = utc.astimezone(to_zone)
                epochTime = time.time()
                #print (ist)
                #print (epochTime)
                #f.write(str(ist) + "," + str(row["value"]) + "\n")
                f.write(str(ist)+","+str(epochTime) + "," + str(row["value"]) + "\n") # File write with EpochTime
            f.close()
        else:
            pass
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

