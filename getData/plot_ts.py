import csv
from datetime import datetime
from matplotlib import pyplot as plt
import statsmodels.api as sm

def main():

	filename = 'SENS_CARBON_MONOXIDE_Prophet.csv'

	with open(filename, 'r') as fp:
		reader = csv.reader(fp)
		list_data = list(reader)

	num_ele = len(list_data)
	list_ts = []; list_index = []
	for ele in range(num_ele):
		list_ts.append(datetime.strptime(list_data[ele][0][:-6], '%Y-%m-%d %H:%M:%S'))
		list_index.append(float(list_data[ele][2]))

	list_te = []
	for ele in range(num_ele):
		time_lag = ((list_ts[ele] - list_ts[0]).total_seconds())/60
		list_te.append(time_lag)
	list_reg = sm.add_constant(list_te)
	model_OLS = sm.OLS(list_index, list_reg)
	results = model_OLS.fit()
	list_OLS = results.predict(list_reg)
	

	fig, ax = plt.subplots()
	ax.scatter(list_ts, list_OLS)
	ax.set_xlabel('time')
	ax.set_ylabel('CO_level')
	fig.savefig('CO_ts.png')
		



	return(0)



if __name__ == '__main__':
	main()
