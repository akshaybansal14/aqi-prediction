from scipy.stats import pearsonr
import pandas as pd
from sklearn.preprocessing import scale
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor
from scipy.stats import pearsonr
import numpy as np

def main():

	start_time = 7; end_time = 19

	df_input = pd.read_csv('sim_parameters.csv')
	num_datapts = df_input.shape[0]

	list_dep = ['co']
	list_features = ['temp', 'rh', 'wd', 'VPH', 'ws', 'wd.sigma', 'cef']

	for time_pt in range(start_time, end_time + 1):
		list_features.append('is_current_hour_' + str(time_pt))
	num_features = len(list_features)
	num_import_features = num_features - (end_time - start_time + 1)

	X_data = []; Y_data = []; X_hour_data = []
	for data_pt in range(num_datapts):
		time_stamp = int(df_input['time'][data_pt].split()[1][:2])

		if(time_stamp >= start_time and time_stamp <= end_time):
			new_pt = [0 for x in range(num_import_features)]
			new_pt_bin = [0 for x in range(end_time - start_time + 1)]
			for i in range(num_import_features):
				new_pt[i] = df_input[list_features[i]][data_pt]
			new_pt_bin[time_stamp - start_time] = 1
			X_data.append(new_pt); X_hour_data.append(new_pt_bin)
			Y_data.append(df_input[list_dep[0]][data_pt])
	
	X_data = scale(X_data, axis = 0).tolist()
	X_data = [x + y for x,y in zip(X_data, X_hour_data)]
	X_data = sm.add_constant(X_data)

	#for i in range(num_features):
	#	print('VIF for ' + list_features[i] + ': ' + str(variance_inflation_factor(X_data, i+1))) 

	list_features = ['constant'] + list_features
	OLS_model = sm.OLS(Y_data, X_data)
	results = OLS_model.fit()
	print(results.summary(xname = list_features))

	best_fit = 0; best_lambda = 0; best_sparse = 0
	for x in range(0):
		lambd = x/1000.0
		ridge = Lasso(alpha = lambd, max_iter = 10e5)
		ridge.fit(X_data, Y_data)
		curr_score = ridge.score(X_data, Y_data)
		print(x, curr_score, ridge.coef_)
		#if(curr_score > best_fit):
		#	best_fit = curr_score
		   # best_sparse = np.sum(lasso.coef_ != 0)
	#print(best_fit, best_lambda, best_sparse)


	return




if __name__ == '__main__':
	main()
