from scipy.stats import pearsonr
import pandas as pd
from sklearn.preprocessing import scale
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor
from scipy.stats import pearsonr
import numpy as np
import matplotlib.pyplot as plt

def main():

	start_time = 7; end_time = 19

	df_input = pd.read_csv('sim_parameters_train.csv')
	num_datapts = df_input.shape[0]

	list_dep = ['co']
	list_features = ['temp', 'rh', 'VPH', 'wd', 'ws', 'cef']

	for time_pt in range(start_time, end_time + 1):
		list_features.append('is_current_hour_' + str(time_pt))
	num_features = len(list_features)
	num_import_features = num_features - (end_time - start_time + 1)

	X_data = []; Y_data = []; X_hour_data = []; Y_caline4 = []
	for data_pt in range(num_datapts):
		time_stamp = int(df_input['time'][data_pt].split()[1][:2])

		if(time_stamp >= start_time and time_stamp <= end_time):
			new_pt = [0 for x in range(num_import_features)]
			new_pt_bin = [0 for x in range(end_time - start_time + 1)]
			for i in range(num_import_features):
				new_pt[i] = df_input[list_features[i]][data_pt]
			new_pt_bin[time_stamp - start_time] = 1
			X_data.append(new_pt); X_hour_data.append(new_pt_bin)
			Y_data.append(df_input[list_dep[0]][data_pt])
			Y_caline4.append(df_input['injection'][data_pt])
	
	X_data = scale(X_data, axis = 0).tolist()
	X_data = [x + y for x,y in zip(X_data, X_hour_data)]

	X_data = sm.add_constant(X_data).tolist()
	#X_data = sm.add_constant(X_data)
	#for i in range(num_features):
	#	print('VIF for ' + list_features[i] + ': ' + str(variance_inflation_factor(X_data, i+1)))

	#inv_cov_mat = (np.dot(X_data.transpose(), X_data))	
	#print(inv_cov_mat)

	list_features = ['constant'] + list_features
	num_features = len(list_features)

	# Extracting training data from the available input data
	Yc_test = Y_data[-21:]; Xc_test = X_data[-21:]
	Y_data[-21:] = []; X_data[-21:] = []    


	OLS_model = sm.OLS(Y_data, X_data)
	results = OLS_model.fit()
	Y_OLS = results.predict(X_data)
	#print(results.summary(xname = list_features))

	start_lambda = 0; end_lambda = 20; range_lambda = 500
	list_lambda = np.linspace(start_lambda, end_lambda, range_lambda).tolist()

	num_data_pts = len(X_data)
	print(num_data_pts)
	num_folds = 10
	train_fraction = 1 / num_folds
	num_train_data_pts = int(train_fraction * num_data_pts)
	
	for curr_lambda in list_lambda: # k-fold cross-validation for each lambda
	
		beta_coef = [0 for x in range(num_features)]
		avg_score_fit = 0; avg_train_score = 0
		for i in range(num_folds):
			X_train = X_data.copy()
			Y_train = Y_data.copy()
			X_test = X_train[i * num_train_data_pts : (i+1) * num_train_data_pts]
			Y_test = Y_train[i * num_train_data_pts : (i+1) * num_train_data_pts]
			X_train[i * num_train_data_pts : (i+1) * num_train_data_pts] = []
			Y_train[i * num_train_data_pts : (i+1) * num_train_data_pts] = [] 	

			ridge = Ridge(alpha = curr_lambda, max_iter = 10e5)
			ridge.fit(X_train, Y_train)
			train_score = ridge.score(X_train, Y_train); test_score = ridge.score(X_test, Y_test)
			list_coeff = ridge.coef_.tolist()
			for feat_index in range(num_features):
				beta_coef[feat_index] += list_coeff[feat_index]
			#print(curr_lambda, i, list_coeff)
			avg_train_score += train_score; avg_score_fit += test_score
			#print(curr_lambda, i+1, train_score, test_score)
			
			X_train = []; Y_train = []; X_test = []; Y_test = []
		beta_coef = [x / num_folds for x in beta_coef]
		print(curr_lambda, avg_train_score / num_folds, avg_score_fit / num_folds, '\n')

	ridge = Ridge(alpha = 0, max_iter = 10e05)
	ridge.fit(X_data, Y_data)
	Y_ridge = ridge.predict(X_data).tolist()
	Y_ridge_pred = ridge.predict(Xc_test).tolist()
	X_ref = [0, 3.5]; Y_ref = [0, 3.5]
	fig, ax = plt.subplots()
	ax.scatter(Y_data, Y_ridge)
	ax.plot(X_ref, Y_ref)
	ax.set_xlabel('Actual Values')
	ax.set_ylabel('Ridge')
	ax.set_title('Comparison of Ridge Estimate with Actual')
	fig.savefig('scatter2.png')
	plt.close(fig)


	# R-squared for Caline-4 model
	y_bar = sum(Y_data)/float(len(Y_data))
	SSE_avg = 0; SSE_fit = 0
	for i in range(num_data_pts):
		SSE_avg += (Y_data[i] - y_bar)**2
		SSE_fit += (Y_data[i] - Y_ridge[i])**2
	
	y_bar = sum(Yc_test)/float(len(Yc_test))
	SSE_avg = 0; SSE_fit = 0
	for i in range(21):
		SSE_avg += (Yc_test[i] - y_bar)**2
		SSE_fit += (Yc_test[i] - Y_ridge_pred[i])**2


	print(1 - (SSE_fit / SSE_avg))

	return



if __name__ == '__main__':
	main()
