import numpy as np
import math

def main():

	# User's input
	d = 5	# dimensions of the input point
	N = 10  # no. of samples to be generated
	coeff_bound = 2
	beta = np.random.uniform(low = -coeff_bound, high = coeff_bound, size = (d,1)) 
	L_sq = d*(coeff_bound**2)

	# Parameter set for multi-variate Gaussian
	mu = np.zeros(shape = d) 	
	k = 5
	W = np.random.normal(size = (d, k))
	D = np.random.uniform(low = 0, high = 1, size = d)
	S = np.dot(W, W.T) + np.diag(D)
	sig = np.dot(np.dot(np.diag(1./np.sqrt(np.diag(S))), S), np.diag(1./np.sqrt(np.diag(S))))
	
	# Parameter set for noise
	t_sq = 0.1

	# Generate data from the above parameters
	X = np.random.multivariate_normal(mu, sig, size = N)	
	ep = math.sqrt(t_sq) * np.random.normal(size = (N,1))

	u, sgv, v = np.linalg.svd(X)
	s = np.zeros(shape = (N,d))
	s[:d,:d] = np.diag(sgv)	
	
	Y = np.dot(X,beta) + ep
	xTx = np.dot(X.T, X)
	xTy = np.dot(X.T, Y)
	I = np.identity(d)
	labd = np.linspace(0, 10, num = 1000) # Set of possible values of adpative conditioner for grid-search

	# Method-I: Assuming \beta has a form similar to the conditioned OLS  
	min_mse = math.inf
	for lbd in labd:
		# variance
		sum_var = 0
		for i in range(d):
			sum_var += (sgv[i]**2) / ((sgv[i]**2 + lbd)**2)
		sum_var = t_sq * sum_var

		beta_est = np.dot(np.linalg.inv(xTx + lbd*I), xTy)

		# bias_sq
		sgv_bias = np.zeros(shape = d)
		for i in range(d):
			sgv_bias[i] = 1 + (sgv[i]**4)/((sgv[i]**2 + lbd)**2) - 2*(sgv[i]**2)/(sgv[i]**2 + lbd)
		
		mse_lbd_est = sum_var + (np.linalg.multi_dot([beta_est.T, v.T, np.diag(sgv_bias), v, beta_est])).item()
		if(mse_lbd_est < min_mse):
			min_mse = mse_lbd_est
			est_A = beta_est
			lb_A = lbd			

	print(est_A, lb_A)

	# Method-II: Assuming an initial \beta and iterating for convergence
	n_iter = 20
	lbd_B = 0
	for i in range(n_iter):
		beta_est = np.dot(np.linalg.inv(xTx + lbd_B*I), xTy)
		min_mse = math.inf
		for lbd in labd:
			val = fp_lambda(lbd, sgv, beta_est, t_sq, v, d)
			if(val < min_mse):
				min_mse = val
				lbd_B = lbd	
	est_B = beta_est
	print(est_B, lbd_B)

	# Method-III: Using the bound on squared-bias
	
	min_mse = math.inf
	for lbd in labd:
		sum_var = 0
		for i in range(d):
			sum_var += (sgv[i]**2) / ((sgv[i]**2 + lbd)**2)
		sum_var = t_sq * sum_var

		upp_bias = L_sq*(1 - (sgv[d-1]**2)/(sgv[d-1]**2 + lbd))**2
		val = sum_var + upp_bias
		if(val < min_mse):
			min_mse = val
			lbd_C = lbd
	est_C = np.dot(np.linalg.inv(xTx + lbd_C*I),xTy)

	# Methods-IV: Using OLS estimator
	est_D = np.dot(np.linalg.inv(xTx), xTy)


	p1 = 0; p2 = 0; p3 = 0; p4 = 0
	for i in range(d):
		p1 += (beta[i][0] - est_A[i][0])**2
		p2 += (beta[i][0] - est_B[i][0])**2 
		p3 += (beta[i][0] - est_C[i][0])**2
		p4 += (beta[i][0] - est_D[i][0])**2	

	print(p3, p4)
		
	return(0)

def fp_lambda(lbd, sgv, beta_est, t_sq, v, d):
	
	sum_var = 0
	for i in range(d):
		sum_var += (sgv[i]**2) / ((sgv[i]**2 + lbd)**2)
	sum_var = t_sq * sum_var

	sgv_bias = np.zeros(shape = d)
	for i in range(d):
		sgv_bias[i] = 1 + (sgv[i]**4)/((sgv[i]**2 + lbd)**2) - 2*(sgv[i]**2)/(sgv[i]**2 + lbd)
		
	val = sum_var + (np.linalg.multi_dot([beta_est.T, v.T, np.diag(sgv_bias), v, beta_est])).item()
	
	return(val)


if __name__ == '__main__':
	main()
