import pandas as pd
import csv
from fbprophet import Prophet
from matplotlib import pyplot as plt

def main():

	filename = 'Climo_CO_Aug_sep.csv'
	with open(filename, 'r') as fp:
		reader = csv.reader(fp, delimiter = ';')
		list_data = list(reader)

	start_index = 0; end_index = len(list_data)

#	start_index = 0; end_index = 39000
	list_future = []

	list_data_new = []

	for i in range(start_index, end_index):
		list_future.append(list_data[i][1])
	df_future = pd.DataFrame(list_future, columns = ['ds'])

	for curr_ele in range(start_index, end_index):
		list_data_new.append([list_data[curr_ele][1], float(list_data[curr_ele][2])])

	#print(list_data_new)
	df_data = pd.DataFrame(list_data_new, columns = ['ds', 'y'])

	m = Prophet(yearly_seasonality = False, weekly_seasonality = 'auto', daily_seasonality = 'auto')
	m.fit(df_data)
	forecast = m.predict(df_future)
	print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']])

	fig1 = m.plot_components(forecast)
	plt.savefig('components_entire_daily_seasonality_new_data_calib.png')
	
	return

if __name__ == '__main__':
	main()
