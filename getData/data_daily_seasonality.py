import csv

def main():

	daily_time_val = '18:00' 

	list_daily_index = [[] for x in range(3)]
	filenames = ['SENS_TEMPERATURE.csv', 'SENS_RELATIVE_HUMIDITY.csv','SENS_CARBON_DIOXIDE.csv']

	i = 0
	for filename in filenames: 
		with open('/Users/akshaybansal/Documents/AQM/Codes/getData/' + filename, 'r') as fp:
			reader = csv.reader(fp)
			#print(reader)
			for row in reader:
				elements = row[0].split()
				date_stamp = elements[0]
				time_stamp = elements[1][0:5] 
				index_val = float(row[2])
				if (time_stamp == daily_time_val):
					list_daily_index[i].append([date_stamp, time_stamp, index_val])
		i += 1

	i = 0
	for filename in filenames:
		write_filename = 'write' + filename
		with open(write_filename, 'wb') as fp:
			writer = csv.writer(fp)
			writer.writerows(list_daily_index[i])
		i += 1
		

	return


if __name__ == "__main__":
	main()
